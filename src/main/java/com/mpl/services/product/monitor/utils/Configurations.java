package com.mpl.services.product.monitor.utils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.commons.zookeeper.ZKWatchEvent;
import com.mpl.commons.zookeeper.ZKWatchListener;
import com.mpl.commons.zookeeper.ZKWatcher;
import com.mpl.commons.zookeeper.Zookeeper;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Configurations implements ZKWatchListener {

    /********************************* Public Classes & Interfaces ********************************/
    public interface ConfigChangeEventListener {
        void onConfigChanged(Configurations newConfig);
    }

    /******************************** Global Variables & Constants ********************************/
    private static final String ZK_ROOT_CONFIG_PATH = "/mpl/service-product-monitor/config";
    private static final MLogger logger = new MLogger(Configurations.class);
    private static Set<ConfigChangeEventListener> changeListeners;
    private static Configurations instance;

    private String zkConnectionString;
    private ZKWatcher zkWatcher;


    private String myName;
    private String myId;
    private int myPort;

    public Boolean getCreateTables() {
        return createTables;
    }

    public void setCreateTables(Boolean createTables) {
        this.createTables = createTables;
    }

    private Boolean createTables;
    private Long monitorInterval;

    private String dbConnection;

    public String getDbConnection() {
        return dbConnection;
    }

    public void setDbConnection(String dbConnection) {
        this.dbConnection = dbConnection;
    }

    public String getDbUser() {
        return dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    private String dbUser;
    private String dbPassword;

    public String getFlockUrl() {
        return flockUrl;
    }

    public void setFlockUrl(String flockUrl) {
        this.flockUrl = flockUrl;
    }

    private String flockUrl;


    /**************************************** Constructors ****************************************/
    public static Configurations getInstance() {
        return instance;
    }

    private Configurations(String zkConnectionString) throws Exception {
        this.zkConnectionString = zkConnectionString;
        changeListeners = new HashSet<>();
        Zookeeper.init(zkConnectionString);
        zkWatcher = new ZKWatcher(ZK_ROOT_CONFIG_PATH, this);
        new ObjectMapper().readerForUpdating(this)
                .readValue(zkWatcher.getData());
    }

    public static void initialize(String zkConnectionString) throws Exception {
        if (instance != null) return;
        instance = new Configurations(zkConnectionString);
    }

    public static void addChangeEventListener(ConfigChangeEventListener listener) {
        changeListeners.add(listener);
    }

    @Override
    public void onChange(ZKWatchEvent zkWatchEvent) {
        try {
            logger.d("Refreshing Configurations");
            new ObjectMapper().readerForUpdating(this)
                    .readValue(zkWatcher.getData());
            logger.d("New Config:", this);
        } catch (Exception e) {
            logger.e("Failed to load config from ZooKeeper", e);
        }

        for (ConfigChangeEventListener l : changeListeners) {
            try {
                l.onConfigChanged(this);
            } catch (Exception e) {
                logger.e("Error in event handler", l.getClass(), e);
            }
        }
    }

    /************************************** Public Functions **************************************/
    public String getZkConnectionString() {
        return zkConnectionString;
    }

    public void setZkConnectionString(String zkConnectionString) {
        this.zkConnectionString = zkConnectionString;
    }

    public ZKWatcher getZkWatcher() {
        return zkWatcher;
    }

    public void setZkWatcher(ZKWatcher zkWatcher) {
        this.zkWatcher = zkWatcher;
    }

    public String getMyName() {
        return myName;
    }

    public void setMyName(String myName) {
        this.myName = myName;
    }

    public String getMyId() {
        return myId;
    }

    public void setMyId(String myId) {
        this.myId = myId;
    }

    public int getMyPort() {
        return myPort;
    }

    public void setMyPort(int myPort) {
        this.myPort = myPort;
    }


    public Long getMonitorInterval() {
        return monitorInterval;
    }

    public void setMonitorInterval(Long monitorInterval) {
        this.monitorInterval = monitorInterval;
    }

    @Override
    public String toString() {
        return "Configurations{" +
                "zkConnectionString='" + zkConnectionString + '\'' +
                ", zkWatcher=" + zkWatcher +
                ", myName='" + myName + '\'' +
                ", myId='" + myId + '\'' +
                ", myPort=" + myPort +
                ", dbConnection=" + dbConnection +
                ", monitorInterval=" + monitorInterval +
                '}';
    }
}

