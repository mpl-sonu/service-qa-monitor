package com.mpl.services.product.monitor.pubsub;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.product.monitor.db.ChallengeServiceImpl;
import com.mpl.services.product.monitor.models.Challenge;

/**
 * sanketh
 */
public class ChallengeEventsConsumer extends ConsumerBase {

    public static final String KEY_CHALLENGE_CREATED = "CHALLENGE_CREATED";
    public static final String KEY_CHALLENGE_EXPIRED = "CHALLENGE_EXPIRED";
    public static final String KEY_CHALLENGE_CANCELLED = "CHALLENGE_CANCELLED";

    private static final ObjectMapper JSON = new ObjectMapper();
    private ChallengeServiceImpl challengeService = new ChallengeServiceImpl();

    private static final MLogger logger = new MLogger(ChallengeEventsConsumer.class);

    @Override
    protected String getConsumerName() {
        return "TierChangeEventsConsumer";
    }

    @Override
    protected String[] getEventsOfInterest() {
        return new String[]{
                KEY_CHALLENGE_CREATED,
                KEY_CHALLENGE_EXPIRED,
                KEY_CHALLENGE_CANCELLED
        };
    }

    @Override
    protected void consumeInternal(long timestamp, ObjectNode payload) throws Exception {
        if (payload.hasNonNull("challenge")) {
            JsonNode node = payload.get("challenge");
            String state = node.get("state").asText();
            if (state.equalsIgnoreCase("CS_ACTIVE")) {
                // on creation
                handleChallengeCreated(timestamp, node);
            } else  {
                // on cancel.reject,expiry
                handleChallengeExpired(timestamp, node);
            }
        }
    }

    private void handleChallengeCreated(long timestamp, JsonNode challNode) throws JsonProcessingException {
        logger.i("CHALLENGE_CREATED");
        logger.i(JSON.writeValueAsString(challNode));
        String currency = challNode.get("currency").asText();
        if (currency.equalsIgnoreCase("CC_CASH") || currency.equalsIgnoreCase("CC_TOKEN")) {
            String challengeId = challNode.get("idStr").asText();
            Challenge challenge = new Challenge();
            challenge.setChallengeId(challNode.get("idStr").asText());
            challenge.setCurrency(challNode.get("currency").asText());
            challenge.setAmount(challNode.get("amount").asDouble());
            challenge.setWinningAmount(challNode.get("winningAmount").asDouble());
            challenge.setCreatedAt(timestamp);
            challenge.setExpiresOn(challNode.get("expiresOn").asLong());
            Challenge chall = challengeService.getChallengeById(challengeId);
            if (chall != null) {
                challengeService.updateChallenge(challenge);
            } else {
                challengeService.createChallenge(challenge);
            }
        }
    }

    private void handleChallengeExpired(long timestamp, JsonNode challNode) throws JsonProcessingException {
        logger.i("CHALLENGE_EXPIRED");
        logger.i(JSON.writeValueAsString(challNode));
        String challengeId = challNode.get("idStr").asText();
        challengeService.expireChallenge(challengeId);
    }


}
