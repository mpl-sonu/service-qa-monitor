package com.mpl.services.product.monitor.db;

import com.mpl.commons.logging.MLogger;
import com.mpl.services.product.monitor.utils.ResourceManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class TransactionServiceImpl implements TransactionService {

    private static final MLogger logger = new MLogger(TransactionServiceImpl.class);
    private static final String TABLE_NAME = "challenge_transactions";
    private static final String QUERY_INSERT = "INSERT INTO " + TABLE_NAME + " (challengeId,transactionId) VALUES (?,?);";
    private static final String QUERY_GET_CHALLENGE_BY_TRANSID = "select challengeId from " + TABLE_NAME + " where transactionId =?";

    @Override
    public void insertTransaction(String challengeId, String transactionId) {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = ResourceManager.getInstance().getConnectionPool().getConnection();
            ps = c.prepareStatement(QUERY_INSERT);
            ps.setString(1, challengeId);
            ps.setString(2, transactionId);
            ps.execute();
        } catch (Exception e) {
            logger.e("Exception inserting transaction", e);
        } finally {
            cleanupConnections(c, rs, ps);
        }
    }

    @Override
    public String getChallengeId(String transactionId) {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = ResourceManager.getInstance().getConnectionPool().getConnection();
            ps = c.prepareStatement(QUERY_GET_CHALLENGE_BY_TRANSID);
            ps.setString(1, transactionId);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString("challengeId");

            }
        } catch (Exception e) {
            logger.e("Exception getting challengeId", e);
        } finally {
            cleanupConnections(c, rs, ps);
        }
        return null;
    }


    protected void cleanupConnections(Connection c, ResultSet rs, PreparedStatement ps) {
        if (rs != null)
            try {
                rs.close();
            } catch (Exception ignored) {
            }
        if (ps != null)
            try {
                ps.close();
            } catch (Exception ignored) {
            }
        if (c != null)
            try {
                c.close();
            } catch (Exception ignored) {
            }
    }
}
