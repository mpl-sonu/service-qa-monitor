package com.mpl.services.product.monitor.db;

public interface TransactionService {

    public void insertTransaction(String challengeId, String transactionId);

    public String getChallengeId(String transactionId);

}
