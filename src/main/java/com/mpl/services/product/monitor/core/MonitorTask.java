package com.mpl.services.product.monitor.core;

import com.mpl.commons.logging.MLogger;
import com.mpl.services.product.monitor.db.ChallengeServiceImpl;
import com.mpl.services.product.monitor.models.Challenge;
import com.mpl.services.product.monitor.utils.Configurations;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MonitorTask extends TimerTask {

    private static final MLogger logger = new MLogger(MonitorTask.class);
    //ChallengeServiceImpl challengeService = new ChallengeServiceImpl();

    public static void startTimer() {
        Timer timer = new Timer();
        Long interval = Configurations.getInstance().getMonitorInterval() * 60 * 1000;
        timer.schedule(new MonitorTask(), 5000, interval);
    }

    @Override
    public void run() {
        logger.i("Runnning monitor task");
        try {
            Long interval = Configurations.getInstance().getMonitorInterval() * 60 * 1000;
            Long endTime = new DateTime().getMillis();
            Long startTime = endTime - interval;
//            List<Challenge> clist = challengeService.getCreatedChallenges(startTime, endTime);
//            List<Challenge> elist = challengeService.getExpiredChallenges(startTime, endTime);
//            List<Challenge> alist = challengeService.getAllAnamolusChallenges();
//            StringBuilder sb = new StringBuilder();
//            sb.append("In Last " + Configurations.getInstance().getMonitorInterval() + " minutes ");
//            sb.append("\n");
//            sb.append("Cash Challenges created : " + clist.size());
//            sb.append("\n");
//            sb.append("Cash Challenges Finished : " + elist.size());
//            sb.append("\n");
//            sb.append("Anomalous Finished Cash Challenges till date : " + alist.size());
//            String result = sendPOST(sb.toString());
//            logger.i("Sent to flock : " + result);
        } catch (Exception e) {
            logger.e("Error running monitor task : ", e);
        }
    }


    private static String sendPOST(String msg) throws IOException {
        String result = "";
        HttpPost post = new HttpPost(Configurations.getInstance().getFlockUrl());
        post.setEntity(new StringEntity("{ \"text\" : \"" + msg + "\"}"));
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {
            result = EntityUtils.toString(response.getEntity());
        }
        return result;
    }
}