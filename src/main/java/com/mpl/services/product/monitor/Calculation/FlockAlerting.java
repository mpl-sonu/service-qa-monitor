package com.mpl.services.product.monitor.Calculation;

import com.amazonaws.util.IOUtils;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class FlockAlerting {
    private static final PoolingHttpClientConnectionManager connectionManager;
    private static ConnectionKeepAliveStrategy connectionStrategy;

    static {
        connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setDefaultMaxPerRoute(24);
        connectionManager.setMaxTotal(100);
        connectionStrategy = new ConnectionKeepAliveStrategy() {
            @Override
            public long getKeepAliveDuration(HttpResponse httpResponse, org.apache.http.protocol.HttpContext httpContext) {
                BasicHeaderElementIterator it = new BasicHeaderElementIterator(httpResponse.headerIterator("Keep-Alive"));
                while (it.hasNext()) {
                    HeaderElement he = it.nextElement();
                    String param = he.getName();
                    String value = he.getValue();
                    if (value != null && param.equalsIgnoreCase
                            ("timeout")) {
                        return Long.parseLong(value) * 1000;
                    }
                }
                return 30 * 1000;
            }
        };
    }

    public static void triggerFlockNotification(String message) {
        try {
            //String url = "https://api.flock.com/hooks/sendMessage/1c2d0765-66c4-45c0-8835-99ba748a48b7"; //Velocity Test
            String url = "https://api.flock.com/hooks/sendMessage/f7c5decb-c5a1-4767-9554-71ef297c94a9"; //Only Me
            if (url == null || url.isEmpty()) return;
            String entity = String.format("{\"text\":\"%s\"}", message);
            HttpPost postRequest = new HttpPost(url);
            postRequest.setEntity(new StringEntity(entity, StandardCharsets.UTF_8));
            CloseableHttpClient client = HttpClients.custom().setConnectionManager(connectionManager)
                    .setKeepAliveStrategy(connectionStrategy)
                    .build();
            CloseableHttpResponse response = client.execute(postRequest);
            StatusLine status = response.getStatusLine();
            HttpEntity responseEntity = response.getEntity();
            String responseString = IOUtils.toString(responseEntity.getContent());
        } catch (IOException ignored) {
        }
    }
}
