package com.mpl.services.product.monitor.db;

import com.mpl.services.product.monitor.models.Challenge;

import java.util.List;

public interface ChallengeService {

    public void createChallenge(Challenge challenge);

    public void updateChallenge(Challenge challenge);

    public void createParticipationChallenge(String challengeId, String currency, Double debit);

    public Challenge getChallengeById(String challengeId);

    public void addCredit(String challengeId, Double credit);

    public void addDebit(String challengeId, Double debit);

    public List<Challenge> getCreatedChallenges(Long startTime, Long endTime);

    public List<Challenge> getExpiredChallenges(Long startTime, Long endTime);

    public void bulkFlagChallenges(List<Challenge> challenges);

    public void expireChallenge(String challengeId);

    public List<Challenge> getAllAnamolusChallenges();

}
