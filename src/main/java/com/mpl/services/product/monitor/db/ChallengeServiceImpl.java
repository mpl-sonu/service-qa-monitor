package com.mpl.services.product.monitor.db;

import com.mpl.commons.logging.MLogger;
import com.mpl.services.product.monitor.models.Challenge;
import com.mpl.services.product.monitor.utils.ResourceManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ChallengeServiceImpl implements ChallengeService {

    private static final MLogger logger = new MLogger(ChallengeServiceImpl.class);
    private static final String TABLE_NAME = "challenges";
    private static final String QUERY_UPDATE = "update challenges set currency=?,amount=?,winningAmount=?,createdAt=?,expiresOn=? where " +
            "challengeId =?";
    private static final String QUERY_INSERT = "INSERT INTO " + TABLE_NAME + " (challengeId, currency,amount,winningAmount,createdAt," +
            "expiresOn,expired) " +
            "VALUES (?,?,?,?,?,?,?);";
    private static final String QUERY_INSERT_PARTICIPATION = "INSERT INTO " + TABLE_NAME + " (challengeId, currency,debit,expired) VALUES" +
            " (?,?,?,?);";
    private static final String QUERY_GET_BY_ID = "select * from " + TABLE_NAME + " where challengeId =?";
    private static final String QUERY_GET_CREDIT = "select credit from " + TABLE_NAME + " where challengeId =?";
    private static final String QUERY_GET_DEBIT = "select debit from " + TABLE_NAME + " where challengeId =?";
    private static final String QUERY_UPDATE_CREDIT = "update challenges set credit = ? where challengeId =?";
    private static final String QUERY_UPDATE_DEBIT = "update challenges set debit = ? where challengeId =?";
    private static final String QUERY_EXPIRE = "update challenges set expired = true where challengeId =?";
    private static final String QUERY_CHALLENGES_CREATED = "select * from " + TABLE_NAME + " where  currency='CC_CASH' and createdAt >=? " +
            "and " +
            "createdAt " +
            "<= ?";
    private static final String QUERY_EXPIRED_BY_TIME = "select * from " + TABLE_NAME + " where  currency='CC_CASH' and expired=true and " +
            "createdAt >=? " +
            "and " +
            "createdAt " +
            "<= ?";
    private static final String QUERY_GET_ALL_EXPIRED = "select * from " + TABLE_NAME + " where  currency='CC_CASH' and expired=true";

    private static final String QUERY_ANOMALY = "select * from " + TABLE_NAME + " where  currency='CC_CASH' and expired=true and " +
            "credit!=debit";


    @Override
    public void createParticipationChallenge(String challengeId, String currency, Double debit) {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = ResourceManager.getInstance().getConnectionPool().getConnection();
            ps = c.prepareStatement(QUERY_INSERT_PARTICIPATION);
            ps.setString(1, challengeId);
            ps.setString(2, currency);
            ps.setDouble(3, debit);
            ps.setBoolean(4, false);
            ps.execute();
            logger.i("Created challenge : " + challengeId);
        } catch (Exception e) {
            logger.e("Exception inserting challenge", e);
        } finally {
            cleanupConnections(c, rs, ps);
        }
    }


    @Override
    public void createChallenge(Challenge challenge) {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = ResourceManager.getInstance().getConnectionPool().getConnection();
            ps = c.prepareStatement(QUERY_INSERT);
            ps.setString(1, challenge.getChallengeId());
            ps.setString(2, challenge.getCurrency());
            ps.setDouble(3, challenge.getAmount());
            ps.setDouble(4, challenge.getWinningAmount());
            ps.setLong(5, challenge.getCreatedAt());
            ps.setLong(6, challenge.getExpiresOn());
            ps.setBoolean(7, false);
            ps.execute();
            logger.i("Created challenge : " + challenge.getChallengeId());
        } catch (Exception e) {
            logger.e("Exception inserting challenge", e);
        } finally {
            cleanupConnections(c, rs, ps);
        }
    }

    @Override
    public void updateChallenge(Challenge challenge) {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = ResourceManager.getInstance().getConnectionPool().getConnection();
            ps = c.prepareStatement(QUERY_UPDATE);
            ps.setString(1, challenge.getCurrency());
            ps.setDouble(2, challenge.getAmount());
            ps.setDouble(3, challenge.getWinningAmount());
            ps.setLong(4, challenge.getCreatedAt());
            ps.setLong(5, challenge.getExpiresOn());
            ps.setString(6, challenge.getChallengeId());
            ps.execute();
            logger.i("updated challenge: " + challenge.getChallengeId());
        } catch (Exception e) {
            logger.e("Exception updating challenge", e);
        } finally {
            cleanupConnections(c, rs, ps);
        }
    }

    @Override
    public Challenge getChallengeById(String challengeId) {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = ResourceManager.getInstance().getConnectionPool().getConnection();
            ps = c.prepareStatement(QUERY_GET_BY_ID);
            ps.setString(1, challengeId);
            rs = ps.executeQuery();
            if (rs.next()) {
                return new Challenge(rs);
            }
        } catch (Exception e) {
            logger.e("Exception getting challengeId", e);
        } finally {
            cleanupConnections(c, rs, ps);
        }
        return null;
    }

    @Override
    public void addCredit(String challengeId, Double credit) {
        Double oldCredit = getCreditByChallengeId(challengeId);
        Double newCredit = oldCredit + credit;
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = ResourceManager.getInstance().getConnectionPool().getConnection();
            ps = c.prepareStatement(QUERY_UPDATE_CREDIT);
            ps.setDouble(1, newCredit);
            ps.setString(2, challengeId);
            ps.execute();
        } catch (Exception e) {
            logger.e("Exception adding credit", e);
        } finally {
            cleanupConnections(c, rs, ps);
        }
    }


    @Override
    public void addDebit(String challengeId, Double debit) {
        Double oldDebit = getDebitByChallengeId(challengeId);
        Double newDebit = oldDebit + debit;
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = ResourceManager.getInstance().getConnectionPool().getConnection();
            ps = c.prepareStatement(QUERY_UPDATE_DEBIT);
            ps.setDouble(1, newDebit);
            ps.setString(2, challengeId);
            ps.execute();
        } catch (Exception e) {
            logger.e("Exception adding debit", e);
        } finally {
            cleanupConnections(c, rs, ps);
        }
    }

    @Override
    public List<Challenge> getCreatedChallenges(Long startTime, Long endTime) {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = ResourceManager.getInstance().getConnectionPool().getConnection();
            ps = c.prepareStatement(QUERY_CHALLENGES_CREATED);
            ps.setLong(1, startTime);
            ps.setLong(2, endTime);
            rs = ps.executeQuery();
            List<Challenge> list = new ArrayList<>();
            while (rs.next()) {
                list.add(new Challenge(rs));
            }
            return list;
        } catch (Exception e) {
            logger.e("Exception getting challengeId", e);
        } finally {
            cleanupConnections(c, rs, ps);
        }
        return null;
    }


    public Double getCreditByChallengeId(String challengeId) {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = ResourceManager.getInstance().getConnectionPool().getConnection();
            ps = c.prepareStatement(QUERY_GET_CREDIT);
            ps.setString(1, challengeId);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getDouble("credit");
            }
        } catch (Exception e) {
            logger.e("Exception getting challengeId", e);
        } finally {
            cleanupConnections(c, rs, ps);
        }
        return null;
    }


    public Double getDebitByChallengeId(String challengeId) {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = ResourceManager.getInstance().getConnectionPool().getConnection();
            ps = c.prepareStatement(QUERY_GET_DEBIT);
            ps.setString(1, challengeId);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getDouble("debit");
            }
        } catch (Exception e) {
            logger.e("Exception getting challengeId", e);
        } finally {
            cleanupConnections(c, rs, ps);
        }
        return null;
    }

    @Override
    public List<Challenge> getExpiredChallenges(Long startTime, Long endTime) {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = ResourceManager.getInstance().getConnectionPool().getConnection();
            ps = c.prepareStatement(QUERY_EXPIRED_BY_TIME);
            ps.setLong(1, startTime);
            ps.setLong(2, endTime);
            rs = ps.executeQuery();
            List<Challenge> list = new ArrayList<>();
            while (rs.next()) {
                list.add(new Challenge(rs));
            }
            return list;
        } catch (Exception e) {
            logger.e("Exception getting challengeId", e);
        } finally {
            cleanupConnections(c, rs, ps);
        }
        return null;
    }

    @Override
    public void bulkFlagChallenges(List<Challenge> challenges) {

    }

    @Override
    public void expireChallenge(String challengeId) {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = ResourceManager.getInstance().getConnectionPool().getConnection();
            ps = c.prepareStatement(QUERY_EXPIRE);
            ps.setString(1, challengeId);
            ps.execute();
        } catch (Exception e) {
            logger.e("Exception expiring  challenge", e);
        } finally {
            cleanupConnections(c, rs, ps);
        }
    }

    @Override
    public List<Challenge> getAllAnamolusChallenges() {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = ResourceManager.getInstance().getConnectionPool().getConnection();
            ps = c.prepareStatement(QUERY_ANOMALY);
            rs = ps.executeQuery();
            List<Challenge> list = new ArrayList<>();
            while (rs.next()) {
                list.add(new Challenge(rs));
            }
            return list;
        } catch (Exception e) {
            logger.e("Exception in getAllExpiredChallenges", e);
        } finally {
            cleanupConnections(c, rs, ps);
        }
        return null;
    }

    protected void cleanupConnections(Connection c, ResultSet rs, PreparedStatement ps) {
        if (rs != null)
            try {
                rs.close();
            } catch (Exception ignored) {
            }
        if (ps != null)
            try {
                ps.close();
            } catch (Exception ignored) {
            }
        if (c != null)
            try {
                c.close();
            } catch (Exception ignored) {
            }
    }
}
