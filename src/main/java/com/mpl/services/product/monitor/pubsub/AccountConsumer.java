package com.mpl.services.product.monitor.pubsub;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.product.monitor.Calculation.FlockAlerting;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class AccountConsumer extends ConsumerBase{

    private static final String USER_ACCOUNT_BALANCE_EVENT = "User Account Balance Updated";
    private static final ObjectMapper JSON = new ObjectMapper();
    List<transactionProcess> allTransactions = new ArrayList<>();
    static Map<String, ResultEntry> result = new HashMap<>();
    LocalDate currentDate;

    public AccountConsumer() {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(new AccountConsumer.ScheduledWorker(), 1, 1, TimeUnit.MINUTES);
    }

    private static final MLogger logger = new MLogger(AccountConsumer.class);

    @Override
    protected String getConsumerName() {
        return "PaymentEventsConsumer";
    }

    @Override
    protected String[] getEventsOfInterest() {
        return new String[]{
                USER_ACCOUNT_BALANCE_EVENT
        };
    }

    @Override
    protected void consumeInternal(long timestamp, ObjectNode payload) throws Exception {
        currentDate = LocalDate.now();
        Calendar calendar = Calendar.getInstance();
        int minutes = calendar.get(Calendar.MINUTE);
        String dateMinutes = currentDate.toString() +"_"+ minutes;
        processAllSources(dateMinutes, timestamp, payload);
    }

    private void processAllSources(String dateMinutes, long timestamp, ObjectNode payload) throws JsonProcessingException {
        logger.i("Copying all Sources");
        logger.i(JSON.writeValueAsString(payload));
        String source = payload.get("Source ").asText();
        String type = payload.get("Type").asText();
        String currency = payload.get("Currency").asText();
        String accountType = payload.get("Account Type").asText();
        double amount = payload.get("Amount").asDouble();
        transactionProcess ts  = new transactionProcess(source, currency, accountType, amount, type, currentDate);
        processTransaction(timestamp, ts);
    }

    private void enterEventDataIntoDB(String source, String type, String accountType, double amount) {
    }

    private void fetchDataFromDB() {
    }

    private void processTransaction(long timestamp, transactionProcess tp) {
        allTransactions.add(tp);
        calculateSummation(allTransactions);
    }

    private void calculateSummation(List<transactionProcess> creDebTransaction) {

        for(transactionProcess tp : creDebTransaction) {
                    if(!result.containsKey(tp.source)) {
                        result.put(tp.source, new ResultEntry(tp.type));
                    }

                    ResultEntry resultEntry = result.get(tp.source);
                    switch(tp.accountType) {
                        case "Bonus Cash":
                    resultEntry.bonus += tp.amount;
                    break;
                case "Deposit Cash":
                    resultEntry.deposit += tp.amount;
                    break;
                case "Tokens":
                    resultEntry.token += tp.amount;
                    break;
                case "Winnings Withdrawable":
                    resultEntry.winnings += tp.amount;
                    break;
                case "In Transit":
                    resultEntry.deposit += tp.amount;
                    break;
            }
        }
    }

    private static void FlockAlert(Map<String, ResultEntry> finalResult) {
        if(!finalResult.isEmpty()) {
            Calendar calendar = Calendar.getInstance();
            String formattedDate = String.format("%d-%d-%d %d:%d",
                    calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE),
                    calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
            FlockAlerting.triggerFlockNotification(String.format("%s \n", formattedDate));
            for (String xyz : finalResult.keySet()) {
                FlockAlerting.triggerFlockNotification(String.format("Events: %s | %s | TOKEN: %.2f | WINNING: %.2f | DEPOSIT: %.2f | BONUS: %.2f",
                        xyz, finalResult.get(xyz).type, finalResult.get(xyz).token, finalResult.get(xyz).winnings, finalResult.get(xyz).deposit, finalResult.get(xyz).bonus));
            }
        }
        finalResult.clear();
    }

    class ScheduledWorker implements Runnable {
        @Override
        public void run() {
            FlockAlert(result);
        }
    }

    private class ResultEntry {
        double bonus;
        double token;
        double deposit;
        double winnings;
        String type;

        public ResultEntry(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return String.format("%s = Bonus: %f Deposit: %f  Winning: %f Token: %f",
                    type, bonus, deposit, winnings, token);
        }
    }

    public class transactionProcess {
        String source;
        String currency;
        String accountType;
        double amount;
        LocalDate currentDate;
        String type;

        transactionProcess(String source, String currency, String accountType, double amount, String type, LocalDate currentDate) {
            this.source = source;
            this.currency = currency;
            this.accountType = accountType;
            this.amount = amount;
            this.currentDate = currentDate;
            this.type = type;
        }
    }
}