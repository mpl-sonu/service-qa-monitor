package com.mpl.services.product.monitor;

import com.mpl.commons.logging.MLogger;
import com.mpl.commons.notification.NotificationManager;
import com.mpl.commons.zookeeper.ZKWatcher;
import com.mpl.services.product.monitor.core.MonitorTask;
import com.mpl.services.product.monitor.utils.Configurations;
import com.mpl.services.product.monitor.utils.ResourceManager;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author sanketh
 */
public class MainClass {
    /******************************** Global Variables & Constants ********************************/
    private static final String PUB_SUB_BASE_PATH = "/mpl/service-qa-monitor/pubsub";
    private static final MLogger logger = new MLogger(MainClass.class);
    private static final String[] DEFAULT_ZK_SERVERS = new String[]{
            "zk1.mpl.internal:2181",
            "zk2.mpl.internal:2181",
            "zk3.mpl.internal:2181"
    };

    /************************************** Public Functions **************************************/
    public static void main(String[] args) {
        try {
            java.security.Security.setProperty("networkaddress.cache.ttl", "10");
            logger.i("Initializing Configurations");
            Configurations.initialize(getZKConnectionString(DEFAULT_ZK_SERVERS));
            Configurations config = Configurations.getInstance();
            logger.i("Config loaded", config);

            //ResourceManager.initialize();

            logger.i("Initializing NotificationManager");
            JSONObject pubSubConfig = readWalkieTalkieConfig(PUB_SUB_BASE_PATH);
            NotificationManager.init(pubSubConfig);
            logger.i("Initialized NotificationManager");

            MonitorTask.startTimer();
            logger.i("Started Product Monitor Service");
            // join()
        } catch (Exception e) {
            e.printStackTrace();
            logger.e("Product Monitor Service Initialization Failed", e);
        }
    }

    /************************************* Core Private Logic *************************************/
    private static String getZKConnectionString(String[] zkServers) {
        if (zkServers.length == 0) return null;
        if (zkServers.length == 1) return zkServers[0];
        List<String> servers = Arrays.asList(zkServers);
        Collections.shuffle(servers);
        StringBuilder sb = new StringBuilder();
        for (String s : servers) sb.append(s).append(",");
        sb.setLength(Math.max(0, sb.length() - 1));
        return sb.toString();
    }

    private static JSONObject readWalkieTalkieConfig(String walkieTalkiePath) throws Exception {
        logger.i("======------------====" + walkieTalkiePath);
        ZKWatcher walkieTalkieWatcher = new ZKWatcher(walkieTalkiePath);
        JSONObject walkieTalkieConfig = new JSONObject(new String(walkieTalkieWatcher.getData()));
        logger.i("==========" + new String(walkieTalkieWatcher.getData()));
        logger.i("=====walkieTalkieConfig=====" + walkieTalkieConfig);
        return walkieTalkieConfig;
    }
}