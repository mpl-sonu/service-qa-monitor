package com.mpl.services.product.monitor.pubsub;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jayway.restassured.http.ContentType;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.product.monitor.Calculation.FlockAlerting;

import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.jayway.restassured.RestAssured.given;

public class AccountEventsConsumer extends ConsumerBase {

    private static final String SEND_TO_DB = "http://localhost:9000/speed";
    private static final String USER_ACCOUNT_BALANCE_EVENT = "User Account Balance Updated";
    private static final ObjectMapper JSON = new ObjectMapper();
    private static final MLogger logger = new MLogger(AccountEventsConsumer.class);
    static Map<String, ResultEntry> result = new HashMap<>();
    List<transactionProcess> allTransactions = new ArrayList<>();
    LocalDate currentDate;

    public AccountEventsConsumer() {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(new AccountEventsConsumer.ScheduledWorker(), 1, 1, TimeUnit.MINUTES);
    }

    private static void FlockAlert(Map<String, ResultEntry> finalResult) {
        if (!finalResult.isEmpty()) {
            Calendar calendar = Calendar.getInstance();
            String formattedDate = String.format("%d-%d-%d %d:%d",
                    calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE),
                    calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
            FlockAlerting.triggerFlockNotification(String.format("%s \n", formattedDate));
//            for (String xyz : finalResult.keySet()) {
//                FlockAlerting.triggerFlockNotification(String.format("Events: %s | %s | TOKEN: %.2f | WINNING: %.2f | DEPOSIT: %.2f | BONUS: %.2f",
//                        xyz, finalResult.get(xyz).type, finalResult.get(xyz).token, finalResult.get(xyz).winnings, finalResult.get(xyz).deposit, finalResult.get(xyz).bonus));
//            }
        }
        finalResult.clear();
    }

    @Override
    protected String getConsumerName() {
        return "PaymentEventsConsumer";
    }

    @Override
    protected String[] getEventsOfInterest() {
        return new String[]{
                USER_ACCOUNT_BALANCE_EVENT
        };
    }

    @Override
    protected void consumeInternal(long timestamp, ObjectNode payload) throws Exception {
        currentDate = LocalDate.now();
        Calendar calendar = Calendar.getInstance();
        int minutes = calendar.get(Calendar.MINUTE);
        String dateMinutes = currentDate.toString() + "_" + minutes;
        processAllSources(dateMinutes, timestamp, payload);
    }

    private void processAllSources(String dateMinutes, long timestamp, ObjectNode payload) throws JsonProcessingException {
        logger.i("Copying all Sources");
        logger.i(JSON.writeValueAsString(payload));
        String source = payload.get("Source ").asText();
        String type = payload.get("Type").asText();
        String currency = payload.get("Currency").asText();
        String accountType = payload.get("Account Type").asText();
        double amount = payload.get("Amount").asDouble();
        //transactionProcess ts  = new transactionProcess(source, currency, accountType, amount, type, currentDate);
        //processTransaction(timestamp, ts);
        enterEventDataIntoDB(source, type, accountType, amount);
    }

    private void enterEventDataIntoDB(String source, String type, String accountType, double amount) {

        //Todo Add Post Call Here
        System.out.println("Post Man Call");
        String Body = "{\"source\":\"" + source + "\",\"type\":\"" + type + "\",\"amount\":\"" + amount + "\",\"accountType\":\"" + accountType + "\"}";
        Response push = post(SEND_TO_DB, Body, "DevUserToken");
    }

    //Todo Write the Code for fetching the data from the database
    private void fetchDataFromDB() {
    }

    public static Response post(String url, String body, String token) {
        Response PostResponse = given()
                .contentType(ContentType.JSON)
                .header(new Header("User-Agent", "mpl-android/1000023 (RV-23)"))
                .header(new Header("Authorization", "Bearer "+token))
                .body(body)
                //.log()
                //.everything()
                .expect()
                .log()
                .ifError()
                .when()
                .post(url);
        return PostResponse;
    }

    public static Response get(String url, String token) {
        Response getResponse = given()
                .contentType(ContentType.JSON)
                .header(new Header("Authorization", "Bearer "+token))
//                .header(new Header("User-Agent", "mpl-android/1000009 (RV-1)"))
                //				.log()
                //				.everything()
//				.expect()
//				.statusCode(retCode)
//				.log()
//				.ifError()
//				.when()
                .get(url);
        return getResponse;
    }

    private class ScheduledWorker implements Runnable {
        @Override
        public void run() {
            FlockAlert(result);
        }
    }

    private class ResultEntry {
        double bonus;
        double token;
        double deposit;
        double winnings;
        String type;

        public ResultEntry(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return String.format("%s = Bonus: %f Deposit: %f  Winning: %f Token: %f",
                    type, bonus, deposit, winnings, token);
        }
    }

    public class transactionProcess {
        String source;
        String currency;
        String accountType;
        double amount;
        LocalDate currentDate;
        String type;

        transactionProcess(String source, String currency, String accountType, double amount, String type, LocalDate currentDate) {
            this.source = source;
            this.currency = currency;
            this.accountType = accountType;
            this.amount = amount;
            this.currentDate = currentDate;
            this.type = type;
        }
    }
}