package com.mpl.services.product.monitor.utils;

import com.mpl.commons.logging.MLogger;
import org.h2.jdbcx.JdbcConnectionPool;

import java.sql.Connection;
import java.sql.Statement;

public class ResourceManager {

    private static final MLogger logger = new MLogger(ResourceManager.class);
    private static Configurations config = Configurations.getInstance();
    private static ResourceManager mInstance;

    private static final String DB_USER = "";
    private static final String DB_PASSWORD = "";

    private JdbcConnectionPool connectionPool;

    private ResourceManager() throws Exception {
        logger.i("Initializing H2 connection pool");
        //String dbConnection = Configurations.getInstance().getDbConnection();
        //this.connectionPool = JdbcConnectionPool.create(dbConnection, DB_USER, DB_PASSWORD);
        if (Configurations.getInstance().getCreateTables()) {
            createTables();
            logger.i("created tables");
        }
        logger.i("H2 connection pool Initialized");
    }

    private void createTables() throws Exception {
        String createQuery1 = "CREATE TABLE challenges (id bigint auto_increment, challengeId varchar(255), currency varchar(255), amount" +
                " decimal, winningAmount decimal, createdAt bigint, expiresOn bigint, credit decimal, debit decimal, expired boolean)";
        String createQuery2 = "CREATE TABLE challenge_transactions (challengeId varchar(255), transactionId  varchar(255))";
        Connection connection = getConnectionPool().getConnection();
        try {
            Statement stmt = connection.createStatement();
            stmt.execute(createQuery1);
            stmt.execute(createQuery2);
            stmt.close();
        } catch (Exception e) {
            logger.e("Error in creating tables : ", e);
        } finally {
            connection.close();
        }
    }

    public static void initialize() throws Exception {
        if (mInstance != null) {
            logger.w("Already Initialized");
            return;
        }
        mInstance = new ResourceManager();
    }

    public static ResourceManager getInstance() {
        return mInstance;
    }

    public JdbcConnectionPool getConnectionPool() {
        return connectionPool;
    }
}
