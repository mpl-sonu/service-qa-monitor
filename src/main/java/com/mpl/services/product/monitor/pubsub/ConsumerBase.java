package com.mpl.services.product.monitor.pubsub;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.commons.notification.subscribe.EventConsumer;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Author: Kaustubh Bhoyar
 */
public abstract class ConsumerBase implements EventConsumer {
    /******************************** Global Variables & Constants ********************************/
    private static final MLogger logger = new MLogger(ConsumerBase.class);

    private Set<String> eventsOfInterest;

    /**************************************** Constructors ****************************************/
    public ConsumerBase() {
        try {
            String[] eoi = getEventsOfInterest();
            if (eoi == null) eoi = new String[0];
            this.eventsOfInterest = new HashSet<>(Arrays.asList(eoi));
        } catch (Exception e) {
            logger.e("failed init of consumer base", e);
        }
    }

    /************************************** Public Functions **************************************/
    @Override
    public boolean consume(JSONObject jsonObject) {
        int responseCode;
        String key = null;
        String consumerName = getConsumerName();
        long startTime = System.currentTimeMillis();
        String internalRequestId = UUID.randomUUID().toString().replace("-", "");
      //  logger.d("INIT", consumerName, internalRequestId, jsonObject);
        try {
            long timestamp = jsonObject.getLong("timestamp");
            key = jsonObject.getString("key");
            if (!eventsOfInterest.contains(key)) {
                logger.d("REJECT", consumerName, internalRequestId, key, 400,
                        (System.currentTimeMillis() - startTime));
                return true;
            }
            ObjectNode payload = new ObjectMapper().readValue(
                    jsonObject.getJSONObject("payload").toString(),
                    ObjectNode.class
            );
            consumeInternal(timestamp, payload);
            responseCode = 200;
        } catch (Exception e) {
            responseCode = 500;
            logger.e("ERROR", consumerName, internalRequestId, key, e);
        }
       // logger.d("COMPLETE", consumerName, internalRequestId, key, responseCode,(System.currentTimeMillis() - startTime));
        return responseCode == 200;
    }

    protected abstract String getConsumerName();

    protected abstract String[] getEventsOfInterest();

    protected abstract void consumeInternal(long timestamp, ObjectNode payload) throws Exception;

    /************************************* Core Private Logic *************************************/
    /************************************** Private Classes ***************************************/
}
