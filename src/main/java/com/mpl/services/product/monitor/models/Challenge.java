package com.mpl.services.product.monitor.models;

import java.sql.ResultSet;

public class Challenge {

    private Integer id;
    private String challengeId;
    private String currency;
    private Double amount;
    private Double winningAmount;
    private Long createdAt;
    private Long expiresOn;
    private Double credit;
    private Double debit;
    private Boolean expired;

    public Challenge(ResultSet rs) throws Exception {
        challengeId = rs.getString("challengeId");
        currency = rs.getString("currency");
        amount = rs.getDouble("amount");
        winningAmount = rs.getDouble("winningAmount");
        createdAt = rs.getLong("createdAt");
        expiresOn = rs.getLong("expiresOn");
        credit = rs.getDouble("credit");
        debit = rs.getDouble("debit");
        expired = rs.getBoolean("expired");
    }

    public Challenge() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(String challengeId) {
        this.challengeId = challengeId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getWinningAmount() {
        return winningAmount;
    }

    public void setWinningAmount(Double winningAmount) {
        this.winningAmount = winningAmount;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getExpiresOn() {
        return expiresOn;
    }

    public void setExpiresOn(Long expiresOn) {
        this.expiresOn = expiresOn;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Boolean getExpired() {
        return expired;
    }

    public void setExpired(Boolean expired) {
        this.expired = expired;
    }

}
